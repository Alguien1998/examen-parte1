from django.contrib import admin

# Register your models here.
from .models import Categoria, Investigacion

admin.site.register(Categoria)
admin.site.register(Investigacion)