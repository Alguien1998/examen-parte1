from django.urls import path

from home import views

urlpatterns = [
    path('list/', views.list, name = "list"),
    path('detail/<int:id>', views.detail, name = "detail"),
    path('create/', views.create, name = "create"),
    path('update/<int:id>/', views.update, name="update"),
    path('delete/<int:id>/', views.delete, name="delete"),

    path('listCC/', views.listCC, name = "listCC"),
    path('listCS/', views.listCS, name = "listCS"),
    path('listCM/', views.listCM, name = "listCM"),
    path('listCN/', views.listCN, name = "listCN"),
    path('listCT/', views.listCT, name = "listCT"),
]