from django.db import models

# Create your models here.

class Categoria(models.Model):
	nombre_categoria = models.CharField(max_length=50)

	def __str__(self):
		return self.nombre_categoria

class Investigacion(models.Model):
	autor = models.CharField(max_length=32)
	clase = models.ForeignKey(Categoria, on_delete=models.CASCADE)
	media = models.FileField(upload_to='myfolder/',null = True, blank=True)
	fecha_subida = models.DateField()
	slug = models.SlugField()


	def __str__(self):
		return self.autor