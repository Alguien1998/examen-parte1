from django.shortcuts import render, redirect

# Create your views here.
from .models import Categoria, Investigacion
from .forms import CategoriaForm, InvestigacionForm

def list(request):
	context = {
	"list_investigacion": Investigacion.objects.all()
	}
	return render(request, "home/list.html", context)


def detail(request, id):
	queryset = Investigacion.objects.get(id = id)
	context = {
	"object": queryset
	}
	return render(request, "home/detail.html", context)

def create(request):
	form = InvestigacionForm(request.POST,request.FILES or None)
	if request.user.is_authenticated:
		error = "User Logged"
		if form.is_valid():
			form.save()
			return redirect("list")
	else:
		error = "User Must Be Logged"

	context = {
	"form": form,
	"menssage": error
	}
	return render(request, "home/create.html", context)

def update(request, id):
	investigacion = Investigacion.objects.get(id=id)
	if  request.method == "GET":
		form = InvestigacionForm(instance=investigacion )
	else:
		form = InvestigacionForm(request.POST, request.FILES, instance=investigacion)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
	"form": form
	}
	return render(request, "home/update.html", context)

def delete(request, id):
	investigacion = Investigacion.objects.get(id=id)
	if request.method == "POST":
		investigacion.delete()
		return redirect("list")
	context = {
		"object": investigacion
	}
	return render(request, "home/delete.html", context)

def listCC(request):
	context = {
	"list_investigacion": Investigacion.objects.filter(clase='1')
	}
	return render(request, "home/listCC.html", context)

def listCM(request):
	context = {
	"list_investigacion": Investigacion.objects.filter(clase='5')
	}
	return render(request, "home/listCM.html", context)

def listCS(request):
	context = {
	"list_investigacion": Investigacion.objects.filter(clase='4')
	}
	return render(request, "home/listCS.html", context)

def listCN(request):
	context = {
	"list_investigacion": Investigacion.objects.filter(clase='3')
	}
	return render(request, "home/listCN.html", context)

def listCT(request):
	context = {
	"list_investigacion": Investigacion.objects.filter(clase='2')
	}
	return render(request, "home/listCT.html", context)
