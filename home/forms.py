from django import forms


from .models import Categoria, Investigacion

class CategoriaForm(forms.ModelForm):
	class Meta:
		model = Categoria
		fields = [
		"nombre_categoria",
		]


class InvestigacionForm(forms.ModelForm):
	class Meta:
		model = Investigacion
		fields = [
		"autor",
		"clase",
		"media",
		"fecha_subida",
		"slug",
		]